const { Console } = require('console');

const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
  });

  readline.question('Choose and option:'+ '\n' + 
  '1. Read package.json:'+ '\n' +    
  '2. Display OS info'+ '\n' +  
  '3. start HTTPS server:'+ '\n' +
  'Type a number: '+ '\n' , choice => {
    readline.close();
    
    if (choice == 1) {
        const fs = require ('fs');

        fs.readFile(__dirname + '/package.json', 'utf-8', (err, content) =>{
            console.log(content)
        });
    }
    
    if (choice == 2) {
        const os = require('os');
        console.log('Getting OS info...')
        console.log( 'SYSTEM MEMORY', (os.totalmem() /1024 / 1024 / 1024).toFixed(2) + 'GB')
        console.log('FREE MOMORY: ', (os.freemem() /1024 / 1024 / 1024).toFixed(2) + 'GB' +'\n' + 
                    'CPU CORES: ', os.cpus().length +'\n' + 
                    'ARCH: ', os.arch() +'\n' + 
                    'PLATFORM: ', process.platform +'\n' + 
                    'RELEASE: ', os.release() +'\n' + 
                    'USER: ', os.userInfo().username
                    )
    }

    if (choice == 3) {
        const http = require('http');
        const server = http.createServer((req, res) => {
            if (req.url === "/") {
                res.write('Hello World!')
                res.end();
            }
        })

        server.listen(3000);
    }
    if (choice > 3 || choice == 0) {
        console.log('invalid option')
    }

  });

